# Documentation for adiuto.org

## Entry points
en: https://docs.adiuto.org

de: https://docs.adiuto.org/de

There is a language switcher on top and in the sidebar.

## Files
en: https://gitlab.com/adiuto/docusaurus/-/tree/master/website/docs\
de: https://gitlab.com/adiuto/docusaurus/-/tree/master/website/i18n/de/docusaurus-plugin-content-docs/current\

### New files

 - new files need be created with the same filename for each language in their respective directoy (listed above).

 - after adding new files the sidebar needs to be eddited to include the new files https://gitlab.com/adiuto/docusaurus/-/blob/master/website/sidebars.js

 - when adding new categories (structural entites) to sidebar.js, they need to be translated in the current.json file for each language.
