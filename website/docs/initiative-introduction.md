---
title: Introduction for initiatives
---

# Initiatives

### Join adiuto.org to manage your donation supply chain.
Create a page for your initiative, or ...

### Go to the initiative overview page and click on the register new initiative button.
... join an existing initiative

Open the page of the initiative you would like to join and use the join initiative link from the right menu. An administrator has to approve your request.

## Initiative management

There are three roles for members of an initiative:

- **Members** can create tasks and manage incoming missions (scan the QR code when the donations successfully arrives). Newly created tasks are deactivated and need to be activated by a coordinator.
- **Coordinator** can also activate and update any tasks for their initiative. (from the Koordinate Tasks link in the right menu)
- **Administrators** can also change the role of members, accept new ones and remove members form the initiative.

On the page of an initiative all links to manage tasks can be found in the right menu. From there you can create tasks and update tasks and manage members. You also have an overview of the missions that are underway.

## Demand

Every tasks has a demand that displays how many of those things are needed. Once a mission with that task leaves the checkout, the demand is reduced accordingly to take the donations in to account that are underway. If a mission is canceled the original demand is restored.

## Drop off

After checkout, the donor collects all the needed donations and goes to the designated location to drop them of. Every mission has a QR Code that can be found along the mission details on adiuto.org (under my missions) or in the email that was send after the checkout process.


At the Drop off point an initiative staffer can scan the QR code with their smartphone and click the displayed link. Clicking the link completes and finishes the mission. (The initiative staffer as to be a member of the initiative on adiuto.org and be logged in.)

<img src="/assets/QRscanWithPhone-initiative-x480.jpg" alt="" />