---
title: Get started
slug: /
---

# Get started

## Multilingual

These help pages are multilingual you can switch your language in the top navigation or in the sidemenu when you're on mobile. Use the **<- Back to main menu** link.
