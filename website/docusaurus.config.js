/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'adiuto.org', 
  tagline: 'Not used its', //For translation this has to be hardcoded where it is used.
  url: 'https://adiuto.gitlab.io',
  baseUrl: '/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'GitLab', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.
  staticDirectories: ['static'],
  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'de'],
    localeConfigs: {
      en: {
        label: 'English',
        direction: 'ltr',
        htmlLang: 'en-US',
      },
      de: {
        label: 'Deutsch',
        direction: 'ltr',
        htmlLang: 'de-DE',
      },
    },
  },
  // Strings in themeConfig are automatically in the translation list (no <Translation> needed)
  themeConfig: {
    navbar: {
      title: 'Documentation for adiuto.org',
      logo: {
        alt: 'adiuto.org Logo',
        src: 'img/logo-shades.svg',
      },
      items: [
        /*{
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },*/
        {
          type: 'localeDropdown',
          position: 'left',
        },
        // No Blog {to: 'blog', label: 'Blog', position: 'left'},
      ],
    },
    // https://docusaurus.io/docs/api/themes/configuration#footer-links
    footer: {
      style: 'light',
      /*links: [
        {
          title: 'Documentation',
          items: [
            {
              label: 'Start',
              to: 'docs',
            },
            {
              label: 'Initiatives',
              to: 'docs/initiatives-introduction',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Stack Overflow',
              href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              html: `
              <a href="https://www.netlify.com" target="_blank" rel="noreferrer noopener" aria-label="Deploys by Netlify">
                <img src="https://www.netlify.com/img/global/badges/netlify-color-accent.svg" alt="Deploys by Netlify" width="114" height="51" />
              </a>
            `,
            },
          ],
        },
      ],*/
      logo: {
        alt: 'adiuto.org Logo',
        src: 'img/logo-shades-large.svg',
        width: 123,
        height: 60,
        href: 'https://adiuto.org',
      },
      copyright: `Copyright © ${new Date().getFullYear()} adiuto.org. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          //routeBasePath makes docs the homepage
          routeBasePath: '/',
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/adiuto/docusaurus/-/tree/master/website',
        },
        /*blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },*/
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
