---
title: Einführung für Initiativen
---
# Initiativen

### Tritt adiuto.org bei, um deine Lieferkette für Spenden zu verwalten.
Erstelle eine Seite für deine Initiative, oder ...

### Gehe zur Übersichtsseite der Initiative und klicke auf die Schaltfläche Neue Initiative registrieren.
.... tritt einer bestehenden Initiative bei

Öffne die Seite der Initiative, der du beitreten möchtest, und verwende den Link Initiative beitreten im rechten Menü. (Gedult, ein:e Administrator:in der Initiative muss deine Anfrage genehmigen.)

## Initiativen verwalten

Es gibt drei Rollen für Mitglieder einer Initiative:

- **Mitglieder** können Aufgaben erstellen und eingehende Missionen abschließen (dazu den QR-Code scannen, wenn die Spenden erfolgreich ankommen). Neu erstellte Aufgaben sind deaktiviert und müssen von Koordinierenden aktiviert werden.
- **Koordinierende** können auch alle Aufgaben für ihre Initiative aktivieren und bearbeiten (das geht unter Aufgaben koordinieren im rechten Menü der initiative).
- **Administrierende** können auch die Rolle von Mitgliedern ändern, neue akzeptieren und Mitglieder aus der Initiative entfernen.

Auf der Seite einer Initiative findest du im rechten Menü alle Verwaltungslinks. Von dort aus kannst Aufgaben erstellen, bearbeiten und Mitglieder verwalten, außerdem kannst du dort auch alle laufenden Missionen einsehen.

## Bedarf

Jede Aufgabe hat einen Bedarf, der zeigt, wie viele dieser Sachen benötigt werden. Sobald eine Mission mit dieser Aufgabe den Checkout verlässt, wird der Bedarf entsprechend reduziert, um die laufenden Spenden zu berücksichtigen. Wenn eine Mission abgebrochen wird, wird der ursprüngliche Bedarf wiederhergestellt.

## Drop off

Nach dem Auschecken packt der Spender alle Spenden zusammen und macht sich auf den Weg zum angegebenen Ort, um sie abzugeben.
Jede Mission hat einen QR-Code, der zusammen mit den Missionsdetails auf adiuto.org (unter Meine Missionen) oder in der E-Mail zu finden ist, die nach dem Bestellvorgang verschickt wurde.
Am Abgabeort kann ein Mitglied der Initiative den QR-Code mit dem Smartphone scannen und auf den angezeigten Link klicken. Durch Klicken auf den Link wird die Mission abgeschlossen und beendet. (Das Mitglied muss Mitglied der Initiative auf adiuto.org und eingeloggt sein.)

<img src="/assets/QRscanWithPhone-initiative-x480.jpg" alt="" />