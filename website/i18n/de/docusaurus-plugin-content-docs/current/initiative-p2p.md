# P2P koordinieren

## Einführung

Mit P2P Peer-to-Peer können individuelle Bedarfe von Initiativen erfasst und von Spendenden erfüllt werden.

<img src="/assets/p2p-TheP2PIdea.png" alt="" />

## 1. Anfragen und Bedürfnisse sammeln

Personen mit einem Bedürfnis kommen zur Initiative und ein Mitglied der Initiative notiert die Anfrage sowie eine E-Mailadresse, oder trägt die Daten gleich in adiuto.org ein.

#### Beispiel

Eine Person benötigt ein Fahrrad für ein Kind und begibt sich zur Initiative. Ein Mitglied der Initiative notiert die Anfrage *Kinderfahrrad 18 Zoll* und die E-Mailadresse *name@example.com* der Person.

## 2. Anfrage auf adiuto.org eintragen

Wenn schon eine Aufgabe (Task) mit der passenden Anfrage vorhanden ist (z.B. weil eine andere Person schon die gleiche Anfrage gestellt hat), kann direkt eine P2P Anfrage (P2P Reference) erstellt werden. Ansonsten muss erst eine passende Aufgabe angelegt werden und erst dann kann die P2P Anfrage erstellt werden.

- Auf die Schaltfläche **+ ADD P2P** klicken
<img src="/assets/p2p-AddP2PButton.png" alt="" />

- In das **Task reference** Feld den Anfang der gesuchten Aufgabe eintippen und den passenden Eintrag (z.B. *(3809) Kinderfahrrad 18 Zoll* aus der Liste auswählen. (Wenn kein passender Begriff angezeigt wird, muss erst eine passende Aufgabe erstellt werden.)
<img src="/assets/p2p-AddTask.png" alt="" />

- In das **E-Mail** Feld die e-Mailadresse eintragen (z.B. *name@example.com*).

## 3. Helfende finden die Anfrage auf adiuto.org

Für Helfende ergibt sich bei der Nutzung von adiuto.org kein Unterschied. Die Aufgabe wird einer Mission hinzugefügt and dann zur Initiative gebracht.

#### Praxis Tipp

Da die Anfragen sehr individuell sein können (z.B. *Kinderfahrrad 18 Zoll*) ist es für die Mitglieder der Initiative nicht immer leicht die ankommenden Spenden der gestellten P2P Anfrage (P2P Reference) zuzuordnen. Es hat sich als sinnvoll erwiesen die Spendenden zu bitten die Aufgabennummer (*3809*) mit zu übergeben.

Dazu kann in das Beschreibungsfeld der Aufgabe (Task) ein entsprechender Vermerk geschrieben werden:

> Hinweis: Bitte die Nummer dieser Aufgabe (3809) auf dem Artikel notieren.

## 4. Helfende bringe die Spenden zu Initiative

Bei der Übergabe der Spenden wird, wie üblich, der QR Code der Mission gescannt. Der QR Code verweist auf eine Seite mit einer Schaltfläche um die Mission abzuschließen. Das klicken der Schaltfläche sorgt auch dafür, dass der Status der P2P Anfrage von *required* auf *delivered* gesetzt wird.

> Zu eine Aufgabe kann es auch mehrere P2P Anfragen geben (z.B. Wenn mehrere Personen ein Kinderfahrrad benötigen). Es wird immer diejenige Anfrage zuerst bearbeitet, die auch zuerst im System eingetragen wurde.

In der Liste der P2P Anfragen erscheint jetzt ein grünes Briefsymbol bei der entsprechenden Aufgabe. Das Anklicken versendet eine Benachrichtigungsmail an die dabei stehende E-Mailadresse.

<img src="/assets/p2p-notification.png" alt="" />

Erklärung zur Übersicht der P2P Anfragen (P2P Referenzen):

<img src="/assets/p2p-filters.png" alt="" />

## 5. Die Spenden übergeben

Die Person kann sich nun zur Initiative begeben und die Spende abholen. Zur Identifikation enthält die Benachrichtigungsemail auch einen QR Code. Dieser Code muss von einem Mitglied der Initiative gescannt werden und verweist auf eine Seite mit der dazugehörigen Anfrage und einer Schaltfläche um die Abholung zu bestätigen. Durch das anklicken dieser Schaltfläche wird der Status der P2P Anfrage (P2P Reference) von *delivered* auf *fetched* gesetzt.
